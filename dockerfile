FROM envoyproxy/envoy:v1.22.0
COPY ./config /tmp
COPY ./config/10-http3/envoy.yaml /etc/envoy/envoy.yaml
RUN chmod go+r /etc/envoy/envoy.yaml

EXPOSE 10000
EXPOSE 9901
EXPOSE 10000/UDP
EXPOSE 443
EXPOSE 80
