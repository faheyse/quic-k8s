KEY_FILE="sfahey.key"
CERT_FILE="sfahey.crt"
HOST="sfahey.com"
CERT_NAME="default-server-secret"
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ${KEY_FILE} -out ${CERT_FILE} -subj "/CN=${HOST}/O=${HOST}" -addext "subjectAltName = DNS:${HOST}"
kubectl create secret tls ${CERT_NAME} --key ${KEY_FILE} --cert ${CERT_FILE} --namespace=default
